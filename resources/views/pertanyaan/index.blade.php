@extends('template.master')

@section('content')
    <div class="ml-3 mt-2">
        <section class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-12">
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">List Pertanyaan</h3>
                    </div>

                    <div class='mt-2 ml-4'>
                    <a href='{{url('/pertanyaan/create')}}' class='text-decoration-none'><button type="button" class="btn btn-primary" >Tambah Data</button></a>
                    </div>

                    <!-- /.card-header -->
                    <div class="card-body">
                        @if (session('berhasil'))
                    <div class='alert alert-success'> {{session('berhasil')}}</div>
                        @endif
                      <table id="example2" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                          <th>No</th>
                          <th>Judul</th>
                          <th>Isi</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse ($data as $row => $pertanyaan)
                                <tr>
                                <td>{{ $row + 1}}</td>
                                <td>{{ $pertanyaan->judul}}</td>
                                <td>{{ $pertanyaan->isi}}</td>
                                <td>
                                    <div>
                                    <a href="/pertanyaan/{{$pertanyaan->id}}" class='text-decoration-none'><button type="button" class="btn btn-info btn-sm" >Detail</button></a>
                                    <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class='text-decoration-none'><button type="button" class="btn btn-warning btn-sm" >Edit</button></a>
                                    <form action="/pertanyaan/{{$pertanyaan->id}}" method='POST'>
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value='Delete' class='btn-danger btn-sm'>
                                    </form>
                                    </div>
                                </td>
                                </tr>
                            @empty
                            <tr><td colspan="5">Tidak ada pertanyaan</td></tr>
                            @endforelse
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Isi</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                      </table>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
      
    </div>
    
@endsection

@push('scripts')
<script src="{{asset ('/Source/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset ('/Source/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush