@extends('template.master')

@section('content')
    <div>
        <div>
        <h3>{{$data->judul}}</h3>
        <p>{{$data->isi}}</p>
        </div>
        <div>
            <p>date created : {{$data->date_created}}</p> 
            <p>date updated : {{$data->date_updated}}</p> 
        </div>
    </div>
    
@endsection