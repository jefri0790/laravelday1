@extends('template.master')

@section('content')
<div class="ml-3 mt-2">
        
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Buat Pertanyaan Baru</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
        <form role="form" action='/pertanyaan/' method='POST'>
                @csrf
            <div class="card-body">
                <div class="form-group">
                <label for="Judul">Judul</label>
                <input type="text" class="form-control" id="Judul" name="Judul" value="{{old('Judul','')}}" placeholder="Masukkan Judul">
                </div>
                <div class="form-group">
                    <label>Isi</label>
                    <textarea class="form-control" rows="4" id="isi" name="isi" value="{{old('isi','')}}" placeholder="Masukkan pertanyaan anda">{{old('isi','')}}</textarea>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

            </form>
            <div class='card-footer'>
                <a href='{{url('/pertanyaan')}}' class='text-decoration-none'><button type="button" class="btn btn-secondary" >Kembali ke Data</button></a>
            </div>
        </div>
</div>


        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
@endsection