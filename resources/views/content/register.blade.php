@extends('template.master')

@section('content')
        <h1>Buat Account Baru</h1>
        <h2>Sign up Form</h2>
        <form method="post" action="{{url('/welcome')}} ">    
        @csrf
        <label for="">Last Name : </label> <input type="text" name ="lastname" id="lastname"> <br><br>
        <label for="">First Name : </label> <input type="text" name="firstname" id="firstname"> <br><br>
        <label for="">Gender : </label> <br><br>
            <input type="radio" name="Male" id="Male"> Male <br>
            <input type="radio" name="Female" id="Female"> Female <br>
            <input type="radio" name="other" id="other"> other <br> <br>

        <label for="">Nationality : </label> 
        <select name="nationality" id="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Singapore">Singapore</option>
            <option value="Australian">Australian</option>
        </select> <br><br>

        <label for="">Language Spoken : </label> <br>
            <input type="checkbox" name="Bahasa" id="Bahasa"> Bahasa Indonesia <br>
            <input type="checkbox" name="English" id="English"> English <br>
            <input type="checkbox" name="Other" id="Other"> Other <br><br>

        <label for="">Bio : </label> <br><br>
            <textarea name="Bio" id="Bio" cols="30" rows="10"></textarea> <br>

        <input type="submit" value="Sign Up"></input>

        </form> 
@endsection