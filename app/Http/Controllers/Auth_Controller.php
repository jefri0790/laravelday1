<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Auth_Controller extends Controller
{
    public function register()
    {
        return view('/content/register');
    }

    public function welcome(Request $req)
    {
        //dd($req->all());
        $name = $req->firstname;
        $last = $req->lastname;
        
        return view('/content/berhasil', ['name' => $name,'lastname' => $last]);
    }
}
