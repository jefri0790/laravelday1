<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Home_Controller extends Controller
{
    public function index()
    {
        return view('/content/index');
    }

    public function Home()
    {
        return view('/content/home');
    }

    public function datatables()
    {
        return view('/content/data-tables');
    }

    public function master()
    {
        return view('/template/master');
    }
}
