<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function index()
    {
        $data = DB::table('pertanyaan')->get();

        return view ('pertanyaan.index',compact('data'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'Judul' => 'required|max:45|unique:pertanyaan',
            'isi' => 'required',
        ]);


        $query = DB::table('pertanyaan')->insert(
            [
                'Judul' => $request['Judul'],
                'isi' =>  $request['isi'],
                'Date_created'=> now()
            ]
        );

        return redirect('/pertanyaan')->with('berhasil','Pertanyaan berhasil Dibuat');
    }

    public function show($id)
    {
        $data = DB::table('pertanyaan')->where('id',$id)->first();
        return view('pertanyaan.show',compact('data'));
    }

    public function edit($id)
    {
        $data = DB::table('pertanyaan')->where('id',$id)->first();
        return view('pertanyaan.edit',compact('data'));
    }

    public function update($id, request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);

        $query = DB::table('pertanyaan')
                    ->where('id', $id)
                    ->update(['isi'=>$request['isi'],
                            'date_updated'=>now()
                            ]);
        return redirect('/pertanyaan')->with('berhasil','Pertanyaan berhasil diedit');
    }

    public function destroy($id)
    {
        $query = DB::table('pertanyaan')
                    ->where('id',$id)
                    ->delete();
        return redirect('/pertanyaan')->with('berhasil','Pertanyaan berhasil dihapus');      
    }
}
